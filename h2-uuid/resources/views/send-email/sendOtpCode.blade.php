<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    .code{
        background-color: rgba(0, 0, 0, 0.048);
        font-size: 32px;
        padding: 10px;
        width: 150px;
        text-align: center;  
    }
</style>
<body>
    <h2>Hi, {{ $name }}</h2>
    <p>Masa Berlaku Kode Otp adalah 5 menit, mohon untuk digunakan secepatnya.</p>
    <div class="code">
        {{ $otpCode }}
    </div>
</body>
</html>