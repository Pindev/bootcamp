<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth:sanctum']],function(){
    Route::post('auth/update-profile',[AuthController::class,'updateProfile']);
    Route::get('auth/get-profile',[AuthController::class,'getProfile']);
});

Route::post('auth/register',[AuthController::class,'register']);
Route::post('auth/login',[AuthController::class,'login']);

Route::post('auth/verification',[AuthController::class,'verifyCode']);
Route::post('auth/regenerate-otp',[AuthController::class,'regenerateOtp']);
Route::post('auth/update-password',[AuthController::class,'updatePassword']);
