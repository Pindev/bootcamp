<?php

namespace App\Http\Controllers\API;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\OtpCode;
use App\Mail\SendCodeOtp;
use Illuminate\Http\Request;
use App\Events\SendNotification;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request){
    try{

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required',
        ]);

        if($validator->fails()){
            return ResponseFormatter::error([
                'error' => $validator->errors()
            ],'Validasi gagal',401);
        }

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $userOtpCode = User::where('email',$data->email)->with('otpCode')->first();
        
        event( new SendNotification($userOtpCode) );

        return ResponseFormatter::success($data,'User berhasil register');
        
        }catch(Exception $error){
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ],'Register Failed',500);
        }
    }

    public function login(Request $request){

        try{
            $validator = Validator::make($request->all(),[
                'email' => 'email|required',
                'password' => 'required|min:8'
            ],[
                'email' => 'Masukkan email yang valid',
                'required' => ':attribute harus di isi'
            ]);

            if($validator->fails()){
                return ResponseFormatter::error([
                    'error' => $validator->errors()
                ],'Validasi Error',401);
            }

            

        $credentials = request(['email','password']);
        // $credentials = $request->only('email','password');

        if(!Auth::attempt($credentials)){
            return ResponseFormatter::error([
                'message' => 'Email atau password tidak terdaftar',
            ], 'Authentication Failed', 500);
        }

        $user = User::where('email',$request->email)->first();

        $token = $user->createToken('authToken')->plainTextToken;
        
        return ResponseFormatter::success([
            'token_access' => $token,
            'token_types' => 'Bearer',
            'user' => $user
        ],'Authenticated');

        }catch(Exception $error){
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error
            ],'Authentication Failed',500);
        }
    }

    public function updatePassword(Request $request){
        try{
    
            $validator = Validator::make($request->all(),[
                'email' => 'required|email|max:255',
                'password' => 'required|min:8',
                'password_new' => 'required|min:8',
                'password_confirmation' => 'required|same:password_new|min:8',
            ]);

            if($validator->fails()){
                return ResponseFormatter::error([
                    'error' => $validator->errors()
                ],'Validasi gagal',401);
            }

            $user = User::where('email',$request->email)->first();

            if(!$user){
                return ResponseFormatter::error([
                    'message' => 'Email tidak ditemukan'
                ],'Update password gagal',500);
            }

            if(Hash::check($request->password, $user->password)){
                $user->password = Hash::make($request->password_new);
                $user->save();

                return ResponseFormatter::success($user,'Password user berhasil di update');
            }else{
                return ResponseFormatter::error([
                    'message' => 'Password lama tidak cocok dengan catatan kami.'
                ],'Update password gagal',401);
            }

            }catch(Exception $error){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => $error,
                ],'Update password Failed',500);
            }
        }

        
    public function updateProfile(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'max:255',
            'photo' => 'image|max:2048',
        ]);

        if($validator->fails()){
            return ResponseFormatter::error([
                'error' => $validator->errors()
            ],'Update Profile Gagal',401);
        }

       
        if($request->name){
            $user =  Auth::user();
            $user->name = $request->name;
            $user->save();
        }
        
        if($request->file('photo')){
            $file = $request->photo->store('assets/profile','public');
            $user = Auth::user();
            $user->photo = $file;
            $user->save();

            return ResponseFormatter::success($user,'data berhasil di update');
        }
    }
    
    public function verifyCode(Request $request){
        $request->validate([
            'code' => 'required|max:6',
        ]);

        $code = OtpCode::where('code',$request->code)->first();

        if(!$code){
            return ResponseFormatter::error(null,'Code otp tidak di temukan',404);
        }

        $currentDate = Carbon::now();

        if($code->expired_date <= $currentDate){
            return ResponseFormatter::error(null,'Code otp telah kadarluarsa',500);
        }

        $user = $code->user;
        $user->email_verified_at = $currentDate;
        $user->save();

        return ResponseFormatter::success($user,'User berhasil terverifikasi');
    }

    public function regenerateOtp(Request $request){
        $request->validate([
            'email' => 'required|email|max:255',
        ]);

        $user = User::where('email',$request->email)->first();

        if(!$user){
            return ResponseFormatter::error(null,'Email tidak di temukan',404);
        }

        $rand_code = mt_rand(100,999) . mt_rand(100,999);

        $date = Carbon::now()->addMinutes(5);
        
        $user->otpCode->update([
            'code' => $rand_code,
            'expired_date' => $date
        ]);
        
        $userOtpCode = User::where('email',$user->email)->with('otpCode')->first();
        
        event( new SendNotification($userOtpCode) );

        return ResponseFormatter::success($user,'Code berhasil di reset');
    }

    public function getProfile(Request $request){
        $id = $request->input('id');
        $name = $request->input('name');

        if($id){
            $data = User::where('id',$id)->get();
            if($data){
                return ResponseFormatter::success($data,'Data berhasil diambil');
            }else{
                return ResponseFormatter::error(null,'Data tidak di temukan',404);
            }
        }

        $data = User::where('name',$name)->get();
        return ResponseFormatter::success($data,'Data berhasil diambil');
    }
}
