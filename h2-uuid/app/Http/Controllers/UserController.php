<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    public function get(){
        $user = User::with('role','otpCode')->get();
        return $user;
    }

    public function otpCode(){
        // untuuk expired_date bisa buat validasi code otp jika waktu sekarang lebih kecil  dari expired_date maka lanjut, jika tidak maka kadarluarsa.
        $rand_code = mt_rand(100,999) . mt_rand(100,999);
        $date = date( "Y-m-d  h:i:s", strtotime( "+5 min" ) );

        $role = OtpCode::create([
            'code' => $rand_code,
            'expired_date' => $date
        ]);
        return 'Berhasil membuat data role <br>' . $role;
    }


    public function verifyEmail(){
        return 'Email anda terverifikasi';
    }
}
