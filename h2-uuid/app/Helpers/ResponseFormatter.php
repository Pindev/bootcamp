<?php 

namespace App\Helpers;

class ResponseFormatter{

    protected static $response = [
        'meta' => [
            'code' => 200,
            'status' => 'success',
            'message' => null,
        ],
        'data' => null
    ];

    public static function success($data = null,$message = null){
        Self::$response['meta']['message'] = $message;
        Self::$response['data'] = $data;

        return response()->json(Self::$response,Self::$response['meta']['code']);
    }

    public static function error($data = null,$message = null,$code = 500){
        Self::$response['meta']['message'] = $message;
        Self::$response['meta']['code'] = $code;
        Self::$response['meta']['status'] = 'error';
        Self::$response['data'] = $data;

        return response()->json(Self::$response,Self::$response['meta']['code']);
    }
}


?>