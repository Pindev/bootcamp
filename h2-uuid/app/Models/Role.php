<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UseUuid;

class Role extends Model
{
    use HasFactory, UseUuid;

    protected $guarded = [];

    public function user(){
        return $this->HasMany(User::class,'role_id');
    }
}
