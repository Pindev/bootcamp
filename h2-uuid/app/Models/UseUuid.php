<?php

namespace App\Models;

use Illuminate\Support\Str;

trait UseUuid
{
    
    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType(){
        return 'string';
    }

}
