<?php

namespace App\Models;

use App\Models\Role;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Carbon\Carbon;


class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;


    public function getRoleId(){
        $role = Role::where('role','user')->first();
        return $role->id;
    }

    
    public function generateOtpCode(){
        $rand_code = mt_rand(100,999) . mt_rand(100,999);
        $date = Carbon::now()->addMinutes(5);

        $role = OtpCode::create([
            'code' => $rand_code,
            'expired_date' => $date
    ]); 

        return $role->id;
    }

    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
            $model->role_id = $model->getRoleId();
            $model->otp_code_id = $model->generateOtpCode();
        });
    }

    public function roleAdmin() {
        if($this->role_id == $this->getRoleId()){
            return false;
        }
        return true;
    }

    // relasi
    public function otpCode(){
        return $this->belongsTo(OtpCode::class,'otp_code_id');
    }

    public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }
    // end relasi


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
}
