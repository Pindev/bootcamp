<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UseUuid;

class OtpCode extends Model
{
    use HasFactory, UseUuid;

    protected $guarded = [];

    public function user(){
        return $this->hasOne(User::class,'otp_code_id');
    }
}
