<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role_id' => null,
            'otp_code_id' => null,
            'name' => 'apin',
            'email' => 'apin82y@gmail.com',
            'password' => Hash::make('123123123'),
        ]);

    }
}
