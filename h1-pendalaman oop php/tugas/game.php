<?php 

trait Hewan{
    public $nama, $jumlahKaki, $keahlian, $darah = 50;

    abstract public function atraksi();
}

trait Fight{
    public $attackPower, $defencePower;
    
    abstract public function diserang();
    abstract public function serang();
}

class Elang {
    use Hewan,Fight;

    // "darah sekarang - attackPower penyerang / defencePower yang diserang"
    public function diserang(){
        $this->darah = $this->darah - 7 / 5;
        echo "elang sedang di serang";
    }

    public function serang(){
        $this->darah = $this->darah - 10 / 8;
        echo "elang sedang menyerang harimau";
    }

    public function atraksi()
    {
        echo "Elang sedang terbang tinggi";
    }

    public function getInfoHewan(){
        $this->nama = 'Elang';
        $this->jumlahKaki = 2;
        $this->keahlian = 'Terbang Tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
        
        echo "Nama :". $this->nama . '<br>';
        echo "Jumlah Kaki :". $this->jumlahKaki . '<br>';
        echo "Keahlian :". $this->keahlian . '<br>';
        echo "Darah :". $this->darah . '<br>';
        echo "Attack Power :". $this->attackPower . '<br>';
        echo "Defence Power :". $this->defencePower . '<br>';
    }
}

class Harimau{
    use Hewan,Fight;

    // "darah sekarang - attackPower penyerang / defencePower yang diserang"
    public function diserang(){
        $this->darah = $this->darah - 10 / 8;
        echo "harimau sedang di serang";
    }

    public function serang(){
        $this->darah = $this->darah - 7 / 5;
        echo "Harimau sedang menyerang elang";
    }

    public function atraksi()
    {
        echo "Harimau sedang Lari Cepat";
    }

    public function getInfoHewan(){
        $this->nama = 'Harimau';
        $this->jumlahKaki = 4;
        $this->keahlian = 'Lari Cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;

        echo "Nama :". $this->nama . '<br>';
        echo "Jumlah Kaki :". $this->jumlahKaki . '<br>';
        echo "Keahlian :". $this->keahlian . '<br>';
        echo "Darah :". $this->darah . '<br>';
        echo "Attack Power :". $this->attackPower . '<br>';
        echo "Defence Power :". $this->defencePower . '<br>';
    }
}

    $elang = new Elang;
    $harimau = new Harimau;
    
    // masukkan fight serang atau di serang
    echo $elang->diserang();
    // echo $harimau->diserang();
    
    echo '<br><br>';

    // masukkan keahlian hewan
    echo $harimau->atraksi();    
    // echo $elang->atraksi();    

    echo '<br><br>';
    echo $elang->getInfoHewan();
    echo '<br><br>';
    echo $harimau->getInfoHewan();
?>